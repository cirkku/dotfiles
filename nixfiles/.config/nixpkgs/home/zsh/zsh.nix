pkgs:
{
  enable = true;
  dotDir =  ".config/zsh";
  initExtra = ''
    PROMPT='%(?.%F{green}√.%F{red}?%?)%f %B%F{240}%1~%f%b %# '
    
    # Variables
    export EDITOR="nvim"
    export TERMINAL="alacritty"
    export BROWSER="qutebrowser"
    export NIX_CONFIG_DIR="$HOME/.config/nixpkgs/"
    export PATH="$HOME/.local/bin:$HOME/bin:$PATH"

    # Clean up
    export XAUTHORITY="$HOME/.Xauthority"
    export GEM_HOME="$HOME/.local/share/gem"
    export LESSHISTFILE=-
    export GTK_RC_FILES="$XDG_CONFIG_HOME"/gtk-1.0/gtkrc
    export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc

    # I honestly don't know what this does
    autoload -U colors && colors
    eval "$(dircolors -b)"
    setopt histignorealldups sharehistory

    # Enable completion
    autoload -Uz compinit
    zstyle ":completion:*" menu select
    zstyle ":completion:*" list-colors ""
    zmodload zsh/complist
    compinit -d $HOME/.cache/zcompdump
    _comp_options+=(globdots)


    # Set vi-mode and bind ctrl + space to accept autosuggestions
    bindkey '^ ' autosuggest-accept
    bindkey '^A' vi-beginning-of-line
    bindkey '^E' vi-end-of-line

        
    '';

    # Tweak settings for history
    history = {
        save = 1000;
        size = 1000;
        path = "$HOME/.cache/zsh_history";
    };

    # Set some aliases
    shellAliases = {
        rcd = ". ranger";
        v = "nvim";
        c = "clear";
        tarx= "tar -xvzf";
        mkdir = "mkdir -vp";
        rm = "rm -rifv";
        mv = "mv -iv";
        cp = "cp -riv";
        cat = "bat --paging=never";
        fzf = "fzf --preview 'bat --color=always --style=numbers --line-range=:500 {}'";
        ls = "exa -a --icons";
        tree = "exa --tree --icons";
        zshrc = "nvim $NIX_CONFIG_DIR/home/zsh/zsh.nix";
        home = "nvim $NIX_CONFIG_DIR/home.nix";
        config = "nvim $NIX_CONFIG_DIR/configuration.nix";
        nvimconf = "nvim $NIX_CONFIG_DIR/home/nvim/nvim.nix";
        rebuild = "sudo nixos-rebuild switch -I nixos-config=$HOME/.config/nixos/configuration.nix";
        ip = "ip --color=auto";
        ipmi = "ipmitool -I lanplus -H 10.0.0.24 -U ADMIN";
        xinstall = "sudo xbps-install -S";
        xupdate = "sudo xbps-install -Suv";
        xremove = "sudo xbps-remove -R";
        wttr = ''clear && curl -s -H "Accept-Language: fi" wttr.in/Huittinen | head -n -3 | tail -n +8'';
    };

    # Source all plugins
    plugins = [
    {
        name = "zsh-syntax-highlighting";
        src = pkgs.fetchFromGitHub {
            owner = "zsh-users";
            repo = "zsh-syntax-highlighting";
            rev = "c7caf57ca805abd54f11f756fda6395dd4187f8a";
            sha256 = "MeuPqDeJpbJi2hT7VUgyQNSmDPY/biUncvyY78IBfzM=";
        };
    }
    {
        name = "auto-ls";
        src = pkgs.fetchFromGitHub {
            owner = "notusknot";
            repo = "auto-ls";
            rev = "62a176120b9deb81a8efec992d8d6ed99c2bd1a1";
            sha256 = "08wgs3sj7hy30x03m8j6lxns8r2kpjahb9wr0s0zyzrmr4xwccj0";
        };
    }
    {
        name = "zsh-autosuggestions";
        src = pkgs.fetchFromGitHub {
            owner = "zsh-users";
            repo = "zsh-autosuggestions";
            rev = "a411ef3e0992d4839f0732ebeb9823024afaaaa8";
            sha256 = "1g3pij5qn2j7v7jjac2a63lxd97mcsgw6xq6k5p7835q9fjiid98";
        };
    }];
}

