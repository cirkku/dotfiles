import os
os.environ['QT_QPA_PLATFORMTHEME'] = 'gtk3'

config.load_autoconfig(False)
config.source('colors.py')

c.tabs.show = 'always'
c.downloads.location.directory = '~/Downloads'
c.url.searchengines = {'DEFAULT': 'https://duckduckgo.com/?q={}', 'am': 'https://www.amazon.com/s?k={}', 'aw': 'https://wiki.archlinux.org/?search={}', 'go': 'https://www.google.com/search?q={}', 'hs': 'https://hoogle.haskell.org/?hoogle={}', 're': 'https://www.reddit.com/r/{}', 'ub': 'https://www.urbandictionary.com/define.php?term={}', 'wiki': 'https://en.wikipedia.org/wiki/{}', 'yt': 'https://www.youtube.com/results?search_query={}'}
c.hints.border = '0px'

# Commands
c.aliases = {'q': 'quit', 'w': 'session-save', 'wq': 'quit --save'}

# Keybinds
config.bind('M', 'hint links spawn mpv --force-window=immediate {hint-url}')
config.bind('Z', 'hint links spawn alacritty -e yt-dlp {hint-url}')
config.bind('t', 'set-cmd-text -s :open -t')
config.bind('xb', 'config-cycle statusbar.show always never')
config.bind('xt', 'config-cycle tabs.show always never')
config.bind('xx', 'config-cycle statusbar.show always never;; config-cycle tabs.show always never')


# downloads
c.downloads.location.directory = '$HOME/Downloads'
c.downloads.location.remember = True

# Fonts
c.fonts.default_family = '"Ttyp0"'
c.fonts.default_size = '11pt'
c.fonts.completion.entry = '11pt "Ttyp0"'
c.fonts.debug_console = '11pt "Ttyp0"'
c.fonts.prompts = 'default_size Ttyp0'
c.fonts.statusbar = '11pt "Ttyp0"'
c.fonts.hints = '20pt "Ttyp0"'

# QtWebEngine Options
config.set('content.cookies.accept', 'all', 'chrome-devtools://*')
config.set('content.cookies.accept', 'all', 'devtools://*')
config.set('content.headers.user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36', '*')
config.set('content.images', True)
config.set('content.javascript.enabled', True)
config.set('content.autoplay', False)
# notfications
config.set('content.notifications.enabled', True, 'https://www.reddit.com')
config.set('content.notifications.enabled', True, 'https://www.youtube.com')

# Default page
c.url.default_page = 'file:///home/tuukka/dotfiles/qutebrowser/.config/qutebrowser/homepage/index.html'
c.url.start_pages = 'file:///home/tuukka/dotfiles/qutebrowser/.config/qutebrowser/homepage/index.html'

# Adblock
c.content.blocking.adblock.lists = [ \
        "https://easylist.to/easylist/easylist.txt", \
        "https://easylist.to/easylist/easyprivacy.txt", \
        "https://secure.fanboy.co.nz/fanboy-cookiemonster.txt", \
        "https://easylist.to/easylist/fanboy-annoyance.txt", \
        "https://secure.fanboy.co.nz/fanboy-annoyance.txt", \
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/annoyances.txt", \
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/filters-2020.txt", \
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/unbreak.txt", \
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/resource-abuse.txt", \
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/privacy.txt", \
        "https://github.com/uBlockOrigin/uAssets/raw/master/filters/filters.txt" \
        ]

c.content.blocking.enabled = True
c.content.blocking.hosts.lists = ['https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts']
c.content.blocking.method = 'both'
